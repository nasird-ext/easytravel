﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace RideHailingApp.Areas.Admin.Helpers
{
    public static class EasyTravelHelper
    {

        public static MvcHtmlString MenuList(this HtmlHelper htmlHelper, Dictionary<string, string> menuItems) 
        {
            
            TagBuilder tagLi;
            TagBuilder tagA;
            TagBuilder tagUl = new TagBuilder("ul");
            tagUl.AddCssClass("nav navbar-nav menu-ul");


            foreach (var item in menuItems)
            {
                tagA = new TagBuilder("a");
                tagLi = new TagBuilder("li");

                tagA.MergeAttribute("href", string.Format("{0}://{1}/{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority, item.Key + "/Index"));
                tagA.InnerHtml = item.Value;

                tagLi.InnerHtml = tagA.ToString();

                tagUl.InnerHtml += tagLi.ToString();
            }

            return MvcHtmlString.Create(tagUl.ToString());
        }
    }
}


