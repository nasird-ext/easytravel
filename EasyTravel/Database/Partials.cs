﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EasyTravel.Database
{
    [MetadataType(typeof(MetaDataUser))]
    public partial class User
    {

    }

    [MetadataType(typeof(MetaDataRideType))]
    public partial class RideType
    {

    }

    [MetadataType(typeof(MetaDataRideState))]
    public partial class RideState
    {

    }

    [MetadataType(typeof(MetaDataDriver))]
    public partial class Driver
    {
        public string PhotoFileName { get; set; }
    }

    [MetadataType(typeof(MetaDataRide))]
    public partial class Ride
    {

    }

    [MetadataType(typeof(MetaDataUser))]
    public partial class AspNetUser
    {
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
