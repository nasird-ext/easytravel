﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTravel.Database
{
    public class MetaDataUser
    {
        public string Id { get; set; }
        [StringLength(255)]
        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        [StringLength(255)]
        [DisplayName("Last Name")]
        [Required]
        public string LastName { get; set; }
        [StringLength(256)]
        [DisplayName("Username")]
        [EmailAddress(ErrorMessage = "Must be a valid email address.")]
        public string UserName { get; set; }
        [DisplayName("Phone Number")]
        [StringLength(maximumLength: 13, MinimumLength = 12, ErrorMessage = "Please provide a valid phone number with country code.")]
        public string PhoneNumber { get; set; }
    }

    public class MetaDataRideType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [DisplayName("Rate per minute")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal PerMinRate { get; set; }
        [DisplayName("Rate per kilometer")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal PerKmRate { get; set; }
        [DisplayName("Base fare")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal BaseFare { get; set; }
        [DisplayName("Active")]
        public bool IsActive { get; set; }
    }

    public class MetaDataRideState
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class MetaDataRide
    {
        public int Id { get; set; }
        [DisplayName("Driver")]
        public int DriverId { get; set; }
        [DisplayName("Passenger")]
        public string PassengerId { get; set; }
        [DisplayName("Ride Type")]
        public int RideTypeId { get; set; }
        [DisplayName("Peak Factor")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public double PeakFactor { get; set; }
        [DisplayName("Ride State")]
        public int RideStateId { get; set; }
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public double Duration { get; set; }
        [DisplayName("Number of Kilometer(s)")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public double Kms { get; set; }
        [DisplayName("Pickup Longitude")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal PickupLongitude { get; set; }
        [DisplayName("Pickup Latitude")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal PickupLatitude { get; set; }
        [DisplayName("Dropoff Longitude")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal DropoffLongitude { get; set; }
        [DisplayName("Dropoff Latitude")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        public decimal DropoffLatitude { get; set; }
    }

    public class MetaDataDriver
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [DisplayName("Driving License Number")]
        [StringLength(10)]
        public string DrivingLicenseNumber { get; set; }
        [DisplayName("Vehicle License Plate")]
        [StringLength(10)]
        public string VehicleLicensePlate { get; set; }
        public string Vehicle { get; set; }
        [DisplayName("Vehicle Model")]
        public int VehicleModel { get; set; }
        [DisplayName("CNIC Number")]
        public string CnicNumber { get; set; }
        [DisplayName("Documents are verified")]
        public bool AreDocumentsVerified { get; set; }
        [DisplayName("Active")]
        public bool IsActive { get; set; }

    }
}
