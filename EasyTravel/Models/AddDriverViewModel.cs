﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EasyTravel.Database;

namespace EasyTravel.Models
{
    public class AddDriverViewModel : Driver
    {
        public string PhotoFileName { get; set; }

        public AddDriverViewModel() : base()
        {
            
        }

        public AddDriverViewModel(Driver driver) {
            this.Id = driver.Id;
            this.UserId = driver.UserId;
            this.DrivingLicenseNumber = driver.DrivingLicenseNumber;
            this.VehicleLicensePlate = driver.VehicleLicensePlate;
            this.Vehicle = driver.Vehicle;
            this.VehicleModel = driver.VehicleModel;
            this.CnicNumber = driver.CnicNumber;
            this.AreDocumentsVerified = driver.AreDocumentsVerified;
            this.IsActive = driver.IsActive;

            this.PhotoFileName = driver.DrivingLicenseNumber + ".png";
        }
    }
}