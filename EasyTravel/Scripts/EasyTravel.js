﻿// Ajax call methods
function ajaxCoreCall(url, contentType, processData, data, cbSuccess, cbFailure, cbError) {
    var errorMsg = "Call failed.";
    $.ajax({
        type: "POST",
        url: url,
        contentType: contentType,
        processData: processData,
        data: data,
        success: function (response) {
            if (cbSuccess) { cbSuccess(response); }
        },
        failure: function (response) {  //alert(errorMsg);
            if (cbFailure) { cbFailure(response); } else { ShowError(errorMsg); console.log(response.responseText) }
        },
        error: function (response) {
            if (cbError) { cbError(response); } else { ShowError(errorMsg); console.log(response.responseText) }
        }
    });
}

function ajaxCall(url, data, cbSuccess, cbFailure, cbError) {
    ajaxCoreCall(url, "application/json; charset=utf-8", true, JSON.stringify(data), cbSuccess, cbFailure, cbError);
}

function ajaxMIMECall(url, data, cbSuccess, cbFailure, cbError) {
    ajaxCoreCall(url, false, false, data, cbSuccess, cbFailure, cbError);
}


function UploadPhoto(elem, action) {
    debugger;
    var fileName = $(elem).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if (elem.files && elem.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
        if (elem.files[0].size > 0 && elem.files[0].size <= 5242880) {
            var formData = new FormData();
            formData.append('file', elem.files[0]);
            formData.append('__RequestVerificationToken', $('#AFT > input').val());
            ajaxMIMECall(action, formData, function (resp) {
                if (resp.IsSuccess === true) {
                    $('#logo-image').attr('src', (photosPath + resp.fileName));
                    $('#LogoImageFileId').val(resp.fileId);
                }
            }, function (fail) {
                alert(fail);
            }, function (err) {
                alert(err);
            });
        }

        else
        {
            alert("Image must not be larger than 5mb.")
        }
    }
    else {
        alert("Invalid image format.");
    }
}
