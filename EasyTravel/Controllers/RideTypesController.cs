﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTravel.Database;

namespace EasyTravel.Controllers
{
    [Authorize]
    public class RideTypesController : Controller
    {
        private EasyTravelEntities db = new EasyTravelEntities();

        // GET: RideTypes
        public ActionResult Index()
        {
            return View(db.RideTypes.ToList());
        }

        // GET: RideTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideType rideType = db.RideTypes.Find(id);
            if (rideType == null)
            {
                return HttpNotFound();
            }
            return View(rideType);
        }

        // GET: RideTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RideTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,PerMinRate,PerKmRate,BaseFare,IsActive")] RideType rideType)
        {
            if (ModelState.IsValid)
            {
                db.RideTypes.Add(rideType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rideType);
        }

        // GET: RideTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideType rideType = db.RideTypes.Find(id);
            if (rideType == null)
            {
                return HttpNotFound();
            }
            return View(rideType);
        }

        // POST: RideTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,PerMinRate,PerKmRate,BaseFare,IsActive")] RideType rideType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rideType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rideType);
        }

        // GET: RideTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideType rideType = db.RideTypes.Find(id);
            if (rideType == null)
            {
                return HttpNotFound();
            }
            return View(rideType);
        }

        // POST: RideTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RideType rideType = db.RideTypes.Find(id);
            db.RideTypes.Remove(rideType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
