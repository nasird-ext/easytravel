﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTravel.Database;

namespace EasyTravel.Controllers
{
    [Authorize]
    public class RidesController : Controller
    {
        private EasyTravelEntities db = new EasyTravelEntities();

        // GET: Rides
        public ActionResult Index()
        {
            var rides = db.Rides.Include(r => r.AspNetUser).Include(r => r.Driver).Include(r => r.RideState).Include(r => r.RideType);
            return View(rides.ToList());
        }

        // GET: Rides/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ride ride = db.Rides.Find(id);
            if (ride == null)
            {
                return HttpNotFound();
            }
            return View(ride);
        }

        // GET: Rides/Create
        public ActionResult Create()
        {
            ViewBag.PassengerId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "UserId");
            ViewBag.RideStateId = new SelectList(db.RideStates, "Id", "Title");
            ViewBag.RideTypeId = new SelectList(db.RideTypes, "Id", "Title");
            return View();
        }

        // POST: Rides/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DriverId,PassengerId,RideTypeId,PeakFactor,RideStateId,Duration,Kms,PickupLongitude,PickupLatitude,DropoffLongitude,DropoffLatitude")] Ride ride)
        {
            if (ModelState.IsValid)
            {
                db.Rides.Add(ride);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PassengerId = new SelectList(db.AspNetUsers, "Id", "Email", ride.PassengerId);
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "UserId", ride.DriverId);
            ViewBag.RideStateId = new SelectList(db.RideStates, "Id", "Title", ride.RideStateId);
            ViewBag.RideTypeId = new SelectList(db.RideTypes, "Id", "Title", ride.RideTypeId);
            return View(ride);
        }

        // GET: Rides/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ride ride = db.Rides.Find(id);
            if (ride == null)
            {
                return HttpNotFound();
            }
            ViewBag.PassengerId = new SelectList(db.AspNetUsers, "Id", "Email", ride.PassengerId);
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "UserId", ride.DriverId);
            ViewBag.RideStateId = new SelectList(db.RideStates, "Id", "Title", ride.RideStateId);
            ViewBag.RideTypeId = new SelectList(db.RideTypes, "Id", "Title", ride.RideTypeId);
            return View(ride);
        }

        // POST: Rides/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DriverId,PassengerId,RideTypeId,PeakFactor,RideStateId,Duration,Kms,PickupLongitude,PickupLatitude,DropoffLongitude,DropoffLatitude")] Ride ride)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ride).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PassengerId = new SelectList(db.AspNetUsers, "Id", "Email", ride.PassengerId);
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "UserId", ride.DriverId);
            ViewBag.RideStateId = new SelectList(db.RideStates, "Id", "Title", ride.RideStateId);
            ViewBag.RideTypeId = new SelectList(db.RideTypes, "Id", "Title", ride.RideTypeId);
            return View(ride);
        }

        // GET: Rides/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ride ride = db.Rides.Find(id);
            if (ride == null)
            {
                return HttpNotFound();
            }
            return View(ride);
        }

        // POST: Rides/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ride ride = db.Rides.Find(id);
            db.Rides.Remove(ride);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
