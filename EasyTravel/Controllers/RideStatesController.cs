﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTravel.Database;

namespace EasyTravel.Controllers
{
    [Authorize]
    public class RideStatesController : Controller
    {
        private EasyTravelEntities db = new EasyTravelEntities();

        // GET: RideStates
        public ActionResult Index()
        {
            return View(db.RideStates.ToList());
        }

        // GET: RideStates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideState rideState = db.RideStates.Find(id);
            if (rideState == null)
            {
                return HttpNotFound();
            }
            return View(rideState);
        }

        // GET: RideStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RideStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title")] RideState rideState)
        {
            if (ModelState.IsValid)
            {
                db.RideStates.Add(rideState);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rideState);
        }

        // GET: RideStates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideState rideState = db.RideStates.Find(id);
            if (rideState == null)
            {
                return HttpNotFound();
            }
            return View(rideState);
        }

        // POST: RideStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title")] RideState rideState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rideState).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rideState);
        }

        // GET: RideStates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RideState rideState = db.RideStates.Find(id);
            if (rideState == null)
            {
                return HttpNotFound();
            }
            return View(rideState);
        }

        // POST: RideStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RideState rideState = db.RideStates.Find(id);
            db.RideStates.Remove(rideState);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
