﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTravel.Database;
using System.IO;
using EasyTravel.Models;

namespace EasyTravel.Controllers
{
    [Authorize]
    public class DriversController : Controller
    {
        private EasyTravelEntities db = new EasyTravelEntities();

        // GET: Drivers
        public ActionResult Index()
        {
            var drivers = db.Drivers.Include(d => d.AspNetUser);
            return View(drivers.ToList());
        }

        // GET: Drivers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // GET: Drivers/Create
        public ActionResult Create()
        {
            IEnumerable<AspNetUser> drivers = db.AspNetUsers.Where(x => x.AspNetRoles.Contains(db.AspNetRoles.FirstOrDefault(y => y.Name == "Driver")));
            ViewBag.UserId = new SelectList(drivers, "Id", "Email");
            return View();
        }

        // POST: Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,DrivingLicenseNumber,VehicleLicensePlate,Vehicle,VehicleModel,CnicNumber,AreDocumentsVerified,IsActive,PhotoFileName")] Driver driver)
        {
            if (ModelState.IsValid)
            {
                try {
                    db.Drivers.Add(driver);
                    db.SaveChanges();
                }
                catch (Exception ex) {
                    throw ex;
                }
                if (!string.IsNullOrEmpty(driver.PhotoFileName)) {

                    string path = Server.MapPath("~/Uploads/Images/DriverPhotos/");
                    string fromFile = path + driver.PhotoFileName;
                    string toFile = String.Format("{0}{1}.png", path, driver.DrivingLicenseNumber);

                    if (System.IO.File.Exists(fromFile))
                        System.IO.File.Move(fromFile, toFile);
                }

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", driver.UserId);
            return View(driver);
        }

        // GET: Drivers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            IEnumerable<AspNetUser> drivers = db.AspNetUsers.Where(x => x.AspNetRoles.Contains(db.AspNetRoles.FirstOrDefault(y => y.Name == "Driver")));
            ViewBag.UserId = new SelectList(drivers, "Id", "Email", driver.UserId);

            driver.PhotoFileName = driver.DrivingLicenseNumber + ".png";

            return View(driver);
        }

        // POST: Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,DrivingLicenseNumber,VehicleLicensePlate,Vehicle,VehicleModel,CnicNumber,AreDocumentsVerified,IsActive,PhotoFileName")] Driver driver)
        {
            if (ModelState.IsValid)
            {
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();

                if (!string.IsNullOrEmpty(driver.PhotoFileName)) {

                    string path = Server.MapPath("~/Uploads/Images/DriverPhotos/");
                    string fromFile = path + driver.PhotoFileName;
                    string toFile = String.Format("{0}{1}.png", path, driver.DrivingLicenseNumber);

                    if (System.IO.File.Exists(fromFile))
                        System.IO.File.Move(fromFile, toFile);
                }

                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", driver.UserId);
            return View(driver);
        }

        // GET: Drivers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // POST: Drivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Driver driver = db.Drivers.Find(id);
            db.Drivers.Remove(driver);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult UploadPhoto (HttpPostedFileBase file)
        {
            string fileName = Guid.NewGuid().ToString() + ".png";

            try {
                if (file != null && file.ContentLength > 0) {

                    string name = file.FileName.Split('\\').Last();

                    var path = Path.Combine(this.Server.MapPath("~/Uploads/Images/DriverPhotos"), fileName);
                    file.SaveAs(path);

                    return Json(new { IsSuccess = true, fileName = fileName }, JsonRequestBehavior.AllowGet);

                }
                else {
                    return Json(new { IsSuccess = false, Message = "Please select some file" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception lex) {
                return Json(new { lex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
